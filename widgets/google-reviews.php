<?php

if ( ! defined( 'ABSPATH' ) ) exit; // If this file is called directly, abort.

use \Elementor\Controls_Manager;
use \Elementor\Group_Control_Border;
use \Elementor\Control_Media;
use \Elementor\Group_Control_Box_Shadow;
use \Elementor\Group_Control_Image_Size;
use \Elementor\Icons_Manager;
use \Elementor\Group_Control_Background;
use \Elementor\Group_Control_Typography;
use \Elementor\Group_Control_Css_Filter;
use \Elementor\Utils;
use \Elementor\Widget_Base;

class Google_Reviews extends Widget_Base {

    protected $google_place_url = "https://maps.googleapis.com/maps/api/place/";

    public function get_name() {
        return 'abdullah-google-reviews';
    }
    public function get_title() {
        return esc_html__( 'Google Reviews', 'abdullah-google-reviews' );
    }
    public function get_icon() {
        return 'eicon-review';
    }
    public function get_categories() {
        return [ 'abdullah-items' ];
    }
    public function get_keywords() {
        return [ 'google reviews', 'reviews', 'google' ];
    }
	public function get_style_depends() {
		return [ 'font-awesome' ];
	}

    protected function register_controls() {

        $abdullah_primary_color = '#7a56ff';

        /**
  		 * Google Reviews Settings
  		 */
  		$this->start_controls_section(
            'abdullah_section_google_reviews_access',
            [
                'label' => esc_html__( 'Access Credentials', 'abdullah-google-reviews' ),
                'tab'   => Controls_Manager::TAB_CONTENT,
            ]
        );

		$this->add_control(
            'abdullah_google_google_map_api_key',
            [
                'label'       => esc_html__( 'Google Map API Key', 'abdullah-google-reviews' ),
                'type'        => Controls_Manager::TEXT,
                'placeholder' => esc_html__( 'Google Map API Key', 'abdullah-google-reviews' ),
                'label_block' => true,
            ]
        );

        $this->add_control(
            'abdullah_google_place_id',
            [
                'label'       => esc_html__( 'Place ID', 'abdullah-google-reviews' ),
                'type'        => Controls_Manager::TEXT,
                'placeholder' => esc_html__( 'Google Place ID', 'abdullah-google-reviews' ),
                'description' => sprintf( __( 'Click %1s HERE %2s to find place ID  ', 'abdullah-google-reviews' ), '<a href="https://developers-dot-devsite-v2-prod.appspot.com/maps/documentation/javascript/examples/full/places-placeid-finder" target="_blank">', '</a>' ),
                'label_block' => true,
            ]
        );

        $this->end_controls_section();

        /**
  		* Layout Setting Content Tab
  		*/
        $this->start_controls_section(
            'abdullah_section_layout',
            [
                'label' => esc_html__( 'Reviews Content', 'abdullah-google-reviews' ),
                'tab'   => Controls_Manager::TAB_CONTENT,
            ]
        );

        $this->add_control(
            'abdullah_reload_reviews',
            [
				'label'   => esc_html__( 'Load Reviews', 'abdullah-google-reviews' ),
				'type'    => Controls_Manager::SELECT,
				'default' => 'abdullah-day',
				'options' => [
					'abdullah-day'      => esc_html__( 'Day', 'abdullah-google-reviews' ),
					'abdullah-hour'     => esc_html__( 'Hour', 'abdullah-google-reviews' ),
					'abdullah-week'     => esc_html__( 'Week', 'abdullah-google-reviews' ),
					'abdullah-month'    => esc_html__( 'Month', 'abdullah-google-reviews' ),
					'abdullah-year'     => esc_html__( 'Year', 'abdullah-google-reviews' )
				]
            ]
		);

        $this->add_control(
			'abdullah_google_reviews_clear_cache',
			[
				'label' => __('Clear Cache', 'abdullah-google-reviews'),
				'type' => Controls_Manager::SWITCHER,
				'return_value' => 'yes',
				'default' => 'no',
				'separator' => 'after',
			]
		);

        $this->add_control(
			'abdullah_google_reviews_show_user_image',
			[
				'label' => __('Reviewer image', 'abdullah-google-reviews'),
				'type' => Controls_Manager::SWITCHER,
				'return_value' => 'yes',
				'default' => 'yes',
			]
		);

		$this->add_control(
			'abdullah_google_reviews_show_name',
			[
				'label' => __('Reviewer Name', 'abdullah-google-reviews'),
				'type' => Controls_Manager::SWITCHER,
				'return_value' => 'yes',
				'default' => 'yes',
			]
		);

		$this->add_control(
			'abdullah_google_reviews_show_date',
			[
				'label' => __('Review Date', 'abdullah-google-reviews'),
				'type' => Controls_Manager::SWITCHER,
				'return_value' => 'yes',
				'default' => 'yes',
			]
		);

        $this->add_control(
			'abdullah_google_reviews_show_rating',
			[
				'label'   => esc_html__( 'Reviewer Rating', 'abdullah-google-reviews' ),
				'type'    => Controls_Manager::SWITCHER,
                'return_value' => 'yes',
				'default' => 'yes',
			]
		);

		$this->add_control(
			'abdullah_google_reviews_rating_icon',
			[
				'label' => __( 'Rating Icon', 'abdullah-google-reviews' ),
				'type' => Controls_Manager::ICONS,
				'label_block' => false,
				'default' => [
					'value' => 'fas fa-star',
					'library' => 'solid',
				],
				'skin' => 'inline',
				'exclude_inline_options' => ['svg'],
				'condition' => [
					'abdullah_google_reviews_show_rating' => 'yes'
				]
			]
		);
		
		$this->add_control(
            'abdullah_google_reviews_rating_layout',
            [
				'label'   => esc_html__( 'Rating Show', 'abdullah-google-reviews' ),
				'type'    => Controls_Manager::SELECT,
				'default' => 'below-author-name',
				'options' => [
					'below-author-name'     => esc_html__( 'Below Author Name', 'abdullah-google-reviews' ),
					'above-description'     => esc_html__( 'Above Description', 'abdullah-google-reviews' ),
					'below-description'     => esc_html__( 'Below Description', 'abdullah-google-reviews' ),
				]
            ]
		);

		$this->add_control(
			'abdullah_google_reviews_show_excerpt',
			[
				'label'   => esc_html__( 'Show Reviewer Content', 'abdullah-google-reviews' ),
				'type'    => Controls_Manager::SWITCHER,
                'return_value' => 'yes',
				'default' => 'yes',
			]
        );

		$this->add_control(
			'abdullah_google_reviews_excerpt_limit',
			[
				'label'     => esc_html__( 'Reviewer Content Limit', 'abdullah-google-reviews' ),
				'type'      => Controls_Manager::NUMBER,
				'default'   => 75,
				'condition' => [
					'abdullah_google_reviews_show_excerpt' => 'yes',
				],
			]
		);

        $this->end_controls_section();

		/**
  		* Carousel Setting Content Tab
  		*/
        $this->start_controls_section(
			'abdullah_section_carousel_settings',
			[
				'label' => esc_html__( 'Carousel Settings', 'abdullah-google-reviews' ),
                'tab'   => Controls_Manager::TAB_CONTENT,
			]
		);

		$slides_per_view = range( 1, 6 );
		$slides_per_view = array_combine( $slides_per_view, $slides_per_view );

		$this->add_control(
            'abdullah_google_reviews_carousel_nav',
            [
                'label'   => esc_html__( 'Navigation Style', 'abdullah-google-reviews' ),
                'type'    => Controls_Manager::SELECT,
                'default' => 'arrows',
                'options' => [
					'arrows'   => esc_html__( 'Arrows', 'abdullah-google-reviews' ),
					'nav-dots' => esc_html__( 'Dots', 'abdullah-google-reviews' ),
					'dynamic-dots' => esc_html__( 'Dynamic Bullets', 'abdullah-google-reviews' ),
					'both'     => esc_html__( 'Arrows and Dots', 'abdullah-google-reviews' ),
					'none'     => esc_html__( 'None', 'abdullah-google-reviews' )                    
                ]
            ]
        );

		$this->add_responsive_control(
			'abdullah_google_reviews_slider_per_view',
			[
				'type'    => Controls_Manager::SELECT,
				'label'   => esc_html__( 'Columns', 'abdullah-google-reviews' ),
				'options' => $slides_per_view,
				'default' => '3',
				'tablet_default' => '2',
				'mobile_default' => '1',
			]
		);

		$this->add_responsive_control(
			'abdullah_google_reviews_column_space',
			[
				'label' => __( 'Column Space', 'abdullah-google-reviews' ),
				'type' => Controls_Manager::SLIDER,
				'size_units' => [ 'px', '%' ],
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 30,
					],
				],
				'default' => [
					'unit' => 'px',
					'size' => 15,
				]
			]
		);

		$this->add_control(
			'abdullah_google_reviews_slides_to_scroll',
			[
				'type'    => Controls_Manager::SELECT,
				'label'   => esc_html__( 'Items to Scroll', 'abdullah-google-reviews' ),
				'options' => $slides_per_view,
				'default'        => 1,
				'tablet_default' => 1,
				'mobile_default' => 1,
			]
		);

		$this->add_control(
			'abdullah_google_reviews_slides_per_column',
			[
				'type'      => Controls_Manager::SELECT,
				'label'     => esc_html__( 'Slides Per Column', 'abdullah-google-reviews' ),
				'options'   => $slides_per_view,
				'default'   => '1',
			]
		);
		
		$this->add_control(
			'abdullah_google_reviews_transition_duration',
			[
				'label'   => esc_html__( 'Transition Duration', 'abdullah-google-reviews' ),
				'type'    => Controls_Manager::NUMBER,
				'default' => 1000
			]
		);

		$this->add_control(
			'abdullah_google_reviews_autoheight',
			[
				'label'     => esc_html__( 'Auto Height', 'abdullah-google-reviews' ),
				'type'      => Controls_Manager::SWITCHER,
				'default'   => 'yes'
			]
		);

		$this->add_control(
			'abdullah_google_reviews_autoplay',
			[
				'label'     => esc_html__( 'Autoplay', 'abdullah-google-reviews' ),
				'type'      => Controls_Manager::SWITCHER,
				'default'   => 'yes'
			]
		);

		$this->add_control(
			'abdullah_google_reviews_autoplay_speed',
			[
				'label'     => esc_html__( 'Autoplay Speed', 'abdullah-google-reviews' ),
				'type'      => Controls_Manager::NUMBER,
				'default'   => 5000,
				'condition' => [
					'abdullah_google_reviews_autoplay' => 'yes'
				]
			]
		);

		$this->add_control(
			'abdullah_google_reviews_loop',
			[
				'label'   => esc_html__( 'Infinite Loop', 'abdullah-google-reviews' ),
				'type'    => Controls_Manager::SWITCHER,
				'default' => 'yes'
			]
		);

		$this->add_control(
			'abdullah_google_reviews_pause',
			[
				'label'     => esc_html__( 'Pause on Hover', 'abdullah-google-reviews' ),
				'type'      => Controls_Manager::SWITCHER,
				'default'   => 'yes',
				'condition' => [
					'abdullah_google_reviews_autoplay' => 'yes'
				]
			]
		);

		$this->add_control(
			'abdullah_google_reviews_slide_centered',
			[
				'label'       => esc_html__( 'Centered Mode Slide', 'abdullah-google-reviews' ),
				'type'        => Controls_Manager::SWITCHER,
				'default'   => 'no',
			]
		);
		
		$this->add_control(
			'abdullah_google_reviews_grab_cursor',
			[
				'label'       => esc_html__( 'Grab Cursor', 'abdullah-google-reviews' ),
				'type'        => Controls_Manager::SWITCHER,
				'default'   => 'no',
			]
		);

		$this->add_control(
			'abdullah_google_reviews_observer',
			[
				'label'       => esc_html__( 'Observer', 'abdullah-google-reviews' ),
				'type'        => Controls_Manager::SWITCHER,
				'default'   => 'no',
			]
		);

		$this->end_controls_section();

		/*
		* Google Reviews Carousel container Styling Section
		*/
		$this->start_controls_section(
			'abdullah_section_google_reviews_container_style',
			[
				'label' => esc_html__( 'Container', 'abdullah-google-reviews' ),
				'tab' => Controls_Manager::TAB_STYLE
			]
		);

		$this->add_control(
			'abdullah_google_reviews_carousel_layout',
			[
				'label' => __( 'Layout', 'abdullah-google-reviews' ),
				'type' => Controls_Manager::SELECT,
				'default' => 'layout-1',
				'options' => [
					'layout-1' => __( 'Layout 1', 'abdullah-google-reviews' ),
					'layout-2' => __( 'Layout 2', 'abdullah-google-reviews' ),
				],
			]
		);

		$this->add_control(
			'abdullah_google_reviews_carousel_container_alignment',
			[
				'label'   => __( 'Alignment', 'abdullah-google-reviews' ),
				'type'    => Controls_Manager::CHOOSE,
				'toggle'  => false,
				'default' => 'abdullah-google-reviews-align-left',
				'options' => [
					'abdullah-google-reviews-align-left'   => [
						'title' => __( 'Left', 'abdullah-google-reviews' ),
						'icon'  => 'eicon-arrow-left'
					],
					'abdullah-google-reviews-align-center' => [
						'title' => __( 'Center', 'abdullah-google-reviews' ),
						'icon'  => 'eicon-arrow-up'
					],
					'abdullah-google-reviews-align-right'  => [
						'title' => __( 'Right', 'abdullah-google-reviews' ),
						'icon'  => 'eicon-arrow-right'
					],
					'abdullah-google-reviews-align-bottom' => [
						'title' => __( 'Bottom', 'abdullah-google-reviews' ),
						'icon'  => 'eicon-arrow-down'
					]
				]
			]
		);

		$this->add_group_control(
			Group_Control_Background::get_type(),
			[
				'name'     => 'abdullah_google_review_carousel_container_background',
				'types'    => [ 'classic', 'gradient' ],
				'selector' => '{{WRAPPER}} .abdullah-google-reviews-wrapper'
			]
		);

		$this->add_group_control(
			Group_Control_Border::get_type(),
			[
				'name'            => 'abdullah_google_review_carousel_container_border',
				'fields_options'  => [
                    'border'      => [
                        'default' => 'solid'
                    ],
                    'width'          => [
                        'default'    => [
							'top'    => '1',
							'right'  => '1',
							'bottom' => '1',
							'left'   => '1'
                        ]
                    ],
                    'color'       => [
                        'default' => '#e3e3e3'
                    ]
				],
				'selector'        => '{{WRAPPER}} .abdullah-google-reviews-wrapper'
			]
		);

		$this->add_responsive_control(
			'abdullah_google_review_carousel_container_radius',
			[
				'label'      => __( 'Border radius', 'abdullah-google-reviews' ),
				'type'       => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', '%', 'em' ],
				'separator'  => 'before',
				'default'    => [
					'top'    => '10',
					'right'  => '10',
					'bottom' => '10',
					'left'   => '10'
				],
				'selectors'  => [
					'{{WRAPPER}} .abdullah-google-reviews-wrapper' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};'
				]
			]
		);

		$this->add_responsive_control(
			'abdullah_google_review_carousel_container_padding',
			[
				'label'      => __( 'Padding', 'abdullah-google-reviews' ),
				'type'       => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', '%', 'em' ],
				'default'    => [
					'top'    => '20',
					'right'  => '20',
					'bottom' => '20',
					'left'   => '20'
				],
				'selectors'  => [
					'{{WRAPPER}} .abdullah-google-reviews-wrapper' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};'
				]
			]
		);

		$this->add_group_control(
			Group_Control_Box_Shadow::get_type(),
			[
				'name'     => 'abdullah_google_review_carousel_container_box_shadow',
				'selector' => '{{WRAPPER}} .abdullah-google-reviews-wrapper'
			]
		);

		$this->end_controls_section();

		/**
		 * Google Reviews Carousel Description Style Section
		 */
		$this->start_controls_section(
			'abdullah_google_reviews_carousel_description_style',
			[
				'label' => esc_html__( 'Reviewer Content', 'abdullah-google-reviews' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_control(
			'abdullah_google_reviews_carousel_description_bg_color',
			[
				'label' => __( 'Background Color', 'abdullah-google-reviews' ),
				'type' => Controls_Manager::COLOR,
				'default' => '#ffffff',
				'selectors' => [
					'{{WRAPPER}} .abdullah-google-reviews-content-wrapper' => 'background: {{VALUE}};',
					'{{WRAPPER}} .abdullah-google-reviews-content-wrapper-arrow::before' => 'color: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'abdullah_google_reviews_carousel_description_color',
			[
				'label' => __( 'Text Color', 'abdullah-google-reviews' ),
				'type' => Controls_Manager::COLOR,
				'default' => '#222222',
				'selectors' => [
					'{{WRAPPER}} .abdullah-google-reviews-description' => 'color: {{VALUE}};',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name' => 'abdullah_google_reviews_carousel_description_typography',
				'label' => __( 'Typography', 'abdullah-google-reviews' ),
				'selector' => '{{WRAPPER}} .abdullah-google-reviews-description',
			]
		);

		$this->add_responsive_control(
			'abdullah_google_reviews_carousel_description_spacing_top',
			[
				'label' => __( 'Top Spacing', 'abdullah-google-reviews' ),
				'type' => Controls_Manager::SLIDER,
				'size_units' => [ 'px' ],
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'default' => [
					'unit' => 'px',
					'size' => 20,
				],
				'selectors' => [
					'{{WRAPPER}} .abdullah-google-reviews-content-wrapper' => 'margin-top: {{SIZE}}{{UNIT}};',
				],
			]
		);

		$this->add_responsive_control(
			'abdullah_google_reviews_carousel_description_spacing_bottom',
			[
				'label' => __( 'Bottom Spacing', 'abdullah-google-reviews' ),
				'type' => Controls_Manager::SLIDER,
				'size_units' => [ 'px' ],
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'default' => [
					'unit' => 'px',
					'size' => 20,
				],
				'selectors' => [
					'{{WRAPPER}} .abdullah-google-reviews-content-wrapper' => 'margin-bottom: {{SIZE}}{{UNIT}};',
				],
			]
		);

		$this->add_responsive_control(
			'abdullah_google_reviews_carousel_description_padding',
			[
				'label' => __( 'Padding', 'abdullah-google-reviews' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', '%', 'em' ],
				'default' => [
					'top' => '0',
					'right' => '0',
					'bottom' => '0',
					'left' => '0',
				],
				'selectors' => [
					'{{WRAPPER}} .abdullah-google-reviews-content-wrapper' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);

		$this->add_responsive_control(
			'abdullah_google_reviews_carousel_description_radius',
			[
				'label' => __( 'Border Radius', 'abdullah-google-reviews' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', '%', 'em' ],
				'default' => [
					'top' => '0',
					'right' => '0',
					'bottom' => '0',
					'left' => '0',
				],
				'selectors' => [
					'{{WRAPPER}} .abdullah-google-reviews-content-wrapper' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Box_Shadow::get_type(),
			[
				'name' => 'abdullah_google_reviews_carousel_description_box_shadow',
				'label' => __( 'Box Shadow', 'abdullah-google-reviews' ),
				'selector' => '{{WRAPPER}} .abdullah-google-reviews-content-wrapper',
			]
		);

		$this->add_group_control(
			Group_Control_Border::get_type(),
			[
				'name'      => 'abdullah_google_reviews_carousel_description_border',
				'selector'  => '{{WRAPPER}} .abdullah-google-reviews-content-wrapper',
			]
		);
		
		$this-> end_controls_section();

		/**
		 * Google Reviews Carousel Rating Style
		 */

		$this->start_controls_section(
			'abdullah_google_reviews_carousel_rating_style',
			[
				'label' => esc_html__( 'Rating', 'abdullah-google-reviews' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_responsive_control(
			'abdullah_google_reviews_carousel_rating_size',
			[
				'label' => __( 'Icon Size', 'abdullah-google-reviews' ),
				'type' => Controls_Manager::SLIDER,
				'size_units' => [ 'px', '%' ],
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 50,
					],
				],
				'default' => [
					'unit' => 'px',
					'size' => 20,
				],
				'selectors' => [
					'{{WRAPPER}} .abdullah-google-reviews-carousel-ratings ul li i' => 'font-size: {{SIZE}}{{UNIT}};',
				],
			]
		);

		$this->add_responsive_control(
			'abdullah_google_reviews_carousel_rating_icon_margin',
			[
				'label' => __( 'Icon Margin', 'abdullah-google-reviews' ),
				'type' => Controls_Manager::SLIDER,
				'size_units' => [ 'px', '%' ],
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 30,
					],
				],
				'default' => [
					'unit' => 'px',
					'size' => 5,
				],
				'selectors' => [
					'{{WRAPPER}} .abdullah-google-reviews-carousel-ratings ul li:not(:last-child) i' => 'margin-right: {{SIZE}}{{UNIT}};',
				],
			]
		);

		$this->add_responsive_control(
			'abdullah_google_reviews_carousel_rating_margin',
			[
				'label' => __( 'Margin', 'abdullah-google-reviews' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', '%', 'em' ],
				'default' => [
					'top' => '0',
					'right' => '0',
					'bottom' => '0',
					'left' => '0',
				],
				'selectors' => [
					'{{WRAPPER}} .abdullah-google-reviews-carousel-ratings' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);

		$this->start_controls_tabs( 'abdullah_google_reviews_carousel_rating_tabs' );

			// normal state rating
			$this->start_controls_tab( 'abdullah_google_reviews_carousel_rating_normal', [ 'label' => esc_html__( 'Normal', 'abdullah-google-reviews' ) ] );

				$this->add_control(
					'abdullah_google_reviews_carousel_rating_normal_color',
					[
						'label' => __( 'Color', 'abdullah-google-reviews' ),
						'type' => Controls_Manager::COLOR,
						'default' => '#222222',
						'selectors' => [
							'{{WRAPPER}} .abdullah-google-reviews-carousel-ratings ul li i' => 'color: {{VALUE}};',
						],
					]
				);

			$this->end_controls_tab();

			// hover state rating
			$this->start_controls_tab( 'abdullah_google_reviews_carousel_rating_active', [ 'label' => esc_html__( 'Active', 'abdullah-google-reviews' ) ] );

				$this->add_control(
					'abdullah_google_reviews_carousel_rating_active_color',
					[
						'label' => __( 'Color', 'abdullah-google-reviews' ),
						'type' => Controls_Manager::COLOR,
						'default' => '#ff5b84',
						'selectors' => [
							'{{WRAPPER}} .abdullah-google-reviews-carousel-ratings ul li.abdullah-google-reviews-ratings-active i' => 'color: {{VALUE}};',
						],
					]
				);

			$this->end_controls_tab();

		$this->end_controls_tabs();

		$this-> end_controls_section();
		
		/**
		 * Google Reviews Carousel Image Style Section
		 */
		$this->start_controls_section(
			'abdullah_google_review_carousel_image_style',
			[
				'label' => esc_html__( 'Reviewer Image', 'abdullah-google-reviews' ),
				'tab'   => Controls_Manager::TAB_STYLE
			]
		);

		$this->add_control(
			'abdullah_google_review_carousel_image_box',
			[
				'label'        => __( 'Image Box', 'abdullah-google-reviews' ),
				'type'         => Controls_Manager::SWITCHER,
				'label_on'     => __( 'ON', 'abdullah-google-reviews' ),
				'label_off'    => __( 'OFF', 'abdullah-google-reviews' ),
				'return_value' => 'yes',
				'default'      => 'no'
			]
		);

		$this->add_responsive_control(
			'abdullah_google_review_carousel_image_box_height',
			[
				'label'       => __( 'Height', 'abdullah-google-reviews' ),
				'type'        => Controls_Manager::SLIDER,
				'size_units'  => [ 'px' ],
				'range'       => [
					'px'      => [
						'min' => 0,
						'max' => 500
					]
				],
				'default'     => [
					'unit'    => 'px',
					'size'    => 80
				],
				'selectors'   => [
					'{{WRAPPER}} .abdullah-google-reviews-thumb'=> 'height: {{SIZE}}{{UNIT}};'
				],
			]
		);

		$this->add_responsive_control(
			'abdullah_google_review_carousel_image_box_width',
			[
				'label'       => __( 'Width', 'abdullah-google-reviews' ),
				'type'        => Controls_Manager::SLIDER,
				'separator'   => 'after',
				'size_units'  => [ 'px' ],
				'range'       => [
					'px'      => [
						'min' => 0,
						'max' => 500
					]
				],
				'default'     => [
					'unit'    => 'px',
					'size'    => 80
				],
				'selectors'   => [
					'{{WRAPPER}} .abdullah-google-reviews-thumb'=> 'width: {{SIZE}}{{UNIT}};',
					'{{WRAPPER}} .abdullah-google-reviews-image-align-left .abdullah-google-reviews-thumb, {{WRAPPER}} .abdullah-google-reviews-image-align-right .abdullah-google-reviews-thumb'=> 'width: {{SIZE}}{{UNIT}};',
					'{{WRAPPER}} .abdullah-google-reviews-image-align-left .abdullah-google-reviews-reviewer, {{WRAPPER}} .abdullah-google-reviews-image-align-right .abdullah-google-reviews-reviewer'=> 'width: calc( 100% - {{SIZE}}{{UNIT}} );',
					'{{WRAPPER}} .abdullah-google-reviews-wrapper.abdullah-google-reviews-align-left .abdullah-google-reviews-content-wrapper-arrow::before'=> 'left: calc( {{SIZE}}{{UNIT}} / 2 );',
					'{{WRAPPER}} .abdullah-google-reviews-wrapper.abdullah-google-reviews-align-right .abdullah-google-reviews-content-wrapper-arrow::before'=> 'right: calc(( {{SIZE}}{{UNIT}} / 2) - 10px);'
				],
			]
		);

		$this->add_group_control(
			Group_Control_Border::get_type(),
			[
				'name'      => 'abdullah_google_review_carousel_image_box_border',
				'selector'  => '{{WRAPPER}} .abdullah-google-reviews-thumb',
			]
		);

		$this->add_responsive_control(
			'abdullah_google_review_carousel_image_box_radius',
			[
				'label'      => __( 'Border radius', 'abdullah-google-reviews' ),
				'type'       => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', '%', 'em' ],
				'default'    => [
					'top'    => '50',
					'right'  => '50',
					'bottom' => '50',
					'left'   => '50',
					'unit'   => '%'
				],
				'selectors'  => [
					'{{WRAPPER}} .abdullah-google-reviews-thumb'   => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
					'{{WRAPPER}} .eabdullah-google-reviews-thumb img' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};'
				]
			]
		);

		$this->add_group_control(
			Group_Control_Box_Shadow::get_type(),
			[
				'name'     => 'abdullah_google_review_carousel_image_box_shadow',
				'selector' => '{{WRAPPER}} .abdullah-google-reviews-thumb'
			]
		);

		$this->add_responsive_control(
			'abdullah_google_review_carousel_image_box_margin_bottom',
			[
				'label'       => __( 'Bottom Spacing', 'abdullah-google-reviews' ),
				'type'        => Controls_Manager::SLIDER,
				'size_units'  => [ 'px' ],
				'range'       => [
					'px'      => [
						'min' => -500,
						'max' => 500
					],
				],
				'default'     => [
					'unit'    => 'px',
					'size'    => 0
				],
				'selectors'   => [
					'{{WRAPPER}} .abdullah-google-reviews-thumb'=> 'margin-bottom: {{SIZE}}{{UNIT}};'
				],
				'condition'   => [
					'abdullah_google_reviews_carousel_container_alignment' => 'abdullah-google-reviews-align-bottom'
				]
			]
		);

		$this-> end_controls_section();

		/**
		 * Google Reviews Style Section
		 */
		$this->start_controls_section(
			'abdullah_google_reviews_carousel_reviewer_style',
			[
				'label' => esc_html__( 'Rivewer', 'abdullah-google-reviews' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_responsive_control(
			'abdullah_google_reviews_carousel_reviewer_padding',
			[
				'label' => __( 'Padding', 'abdullah-google-reviews' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', '%', 'em' ],
				'default' => [
					'top' => '0',
					'right' => '0',
					'bottom' => '0',
					'left' => '0',
				],
				'selectors' => [
					'{{WRAPPER}} .abdullah-google-reviews-reviewer-wrapper' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);

		$this->add_responsive_control(
			'abdullah_google_reviews_carousel_reviewer_spacing',
			[
				'label' => __( 'Spacing', 'abdullah-google-reviews' ),
				'type' => Controls_Manager::SLIDER,
				'size_units' => [ 'px' ],
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'default' => [
					'unit' => 'px',
					'size' => 20,
				],
				'selectors' => [
					'{{WRAPPER}} .abdullah-google-reviews-wrapper.abdullah-google-reviews-align-left .abdullah-google-reviews-reviewer-wrapper .abdullah-google-reviews-reviewer' => 'padding-left: {{SIZE}}{{UNIT}};',
					'{{WRAPPER}} .abdullah-google-reviews-wrapper.abdullah-google-reviews-align-right .abdullah-google-reviews-reviewer-wrapper .abdullah-google-reviews-reviewer' => 'padding-right: {{SIZE}}{{UNIT}};',
				],
				'condition' => [
					'abdullah_google_reviews_carousel_container_alignment' => ['abdullah-google-reviews-align-left', 'abdullah-google-reviews-align-right'],
				]
			]
		);

		/**
		 * Google Reviews Title Style Section
		 */

		$this->add_control(
			'abdullah_google_reviews_carousel_title_style',
			[
				'label' => __( 'Reviewer Title', 'abdullah-google-reviews' ),
				'type' => Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name' => 'abdullah_google_reviews_carousel_title_typography',
				'label' => __( 'Typography', 'abdullah-google-reviews' ),
				'selector' => '{{WRAPPER}} .abdullah-author-name a',
			]
		);

		$this->add_control(
			'abdullah_google_reviews_carousel_title_color',
			[
				'label' => __( 'Color', 'abdullah-google-reviews' ),
				'type' => Controls_Manager::COLOR,
				'default' => '#222222',
				'selectors' => [
					'{{WRAPPER}} .abdullah-author-name a' => 'color: {{VALUE}};',
				],
			]
		);

		$this->add_responsive_control(
			'abdullah_google_reviews_carousel_title_margin',
			[
				'label' => __( 'Margin', 'abdullah-google-reviews' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', '%', 'em' ],
				'default' => [
					'top' => '0',
					'right' => '0',
					'bottom' => '0',
					'left' => '0',
				],
				'selectors' => [
					'{{WRAPPER}} .abdullah-author-name' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);

		/**
		 * Google Reviews Designation Style Section
		 */

		$this->add_control(
			'abdullah_google_reviews_carousel_designation_style',
			[
				'label' => __( 'Reviewer Date', 'abdullah-google-reviews' ),
				'type' => Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name' => 'abdullah_google_reviews_carousel_date',
				'label' => __( 'Typography', 'abdullah-google-reviews' ),
				'selector' => '{{WRAPPER}} .abdullah-google-reviews-date',
			]
		);

		$this->add_control(
			'abdullah_google_reviews_carousel_designation_color',
			[
				'label' => __( 'Color', 'abdullah-google-reviews' ),
				'type' => Controls_Manager::COLOR,
				'default' => '#222222',
				'selectors' => [
					'{{WRAPPER}} .abdullah-google-reviews-date' => 'color: {{VALUE}};',
				],
			]
		);

		$this->add_responsive_control(
			'abdullah_google_reviews_carousel_designation_margin',
			[
				'label' => __( 'Margin', 'abdullah-google-reviews' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', '%', 'em' ],
				'default' => [
					'top' => '0',
					'right' => '0',
					'bottom' => '0',
					'left' => '0',
				],
				'selectors' => [
					'{{WRAPPER}} .abdullah-google-reviews-date' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);

		$this-> end_controls_section();

		/**
		 * Arrows Style
		 */
		$this->start_controls_section(
            'abdullah_google_reviews_carousel_nav_arrow',
            [
                'label'     => esc_html__( 'Arrows', 'abdullah-google-reviews' ),
                'tab'       => Controls_Manager::TAB_STYLE,
                'condition' => [
                    'abdullah_google_reviews_carousel_nav' => ['arrows', 'both'],
                ],
            ]
        );

        $this->add_responsive_control(
            'abdullah_google_reviews_carousel_nav_arrow_box_size',
            [
                'label'      => __( 'Box Size', 'abdullah-google-reviews' ),
                'type'       => Controls_Manager::SLIDER,
                'size_units' => ['px'],
                'range'      => [
                    'px' => [
                        'min' => 0,
                        'max' => 200,
                    ],
                ],
                'default'    => [
                    'unit' => 'px',
                    'size' => 50,
                ],
                'selectors'  => [
                    '{{WRAPPER}} .abdullah-google-reviews-carousel-wrapper .abdullah-carousel-nav-prev' => 'width: {{SIZE}}{{UNIT}}; height: {{SIZE}}{{UNIT}};',
                    '{{WRAPPER}} .abdullah-google-reviews-carousel-wrapper .abdullah-carousel-nav-next' => 'width: {{SIZE}}{{UNIT}}; height: {{SIZE}}{{UNIT}};',
                ],
            ]
        );

        $this->add_responsive_control(
            'abdullah_google_reviews_carousel_nav_arrow_icon_size',
            [
                'label'      => __( 'Icon Size', 'abdullah-google-reviews' ),
                'type'       => Controls_Manager::SLIDER,
                'size_units' => ['px'],
                'range'      => [
                    'px' => [
                        'min' => 0,
                        'max' => 50,
                    ],
                ],
                'default'    => [
                    'unit' => 'px',
                    'size' => 16,
                ],
                'selectors'  => [
                    '{{WRAPPER}} .abdullah-google-reviews-carousel-wrapper .abdullah-carousel-nav-prev i' => 'font-size: {{SIZE}}{{UNIT}};',
                    '{{WRAPPER}} .abdullah-google-reviews-carousel-wrapper .abdullah-carousel-nav-next i' => 'font-size: {{SIZE}}{{UNIT}};',
                ],
            ]
        );
        
        $this->add_control(
            'abdullah_google_reviews_carousel_prev_arrow_position',
            [
                'label'        => __( 'Previous Arrow Position', 'abdullah-google-reviews' ),
                'type'         => Controls_Manager::POPOVER_TOGGLE,
                'label_off'    => __( 'Default', 'abdullah-google-reviews' ),
                'label_on'     => __( 'Custom', 'abdullah-google-reviews' ),
                'return_value' => 'yes',
                'default'      => 'yes',
            ]
        );
        
        $this->start_popover();

            $this->add_responsive_control(
                'abdullah_google_reviews_carousel_prev_arrow_position_x_offset',
                [
                    'label'      => __( 'X Offset', 'abdullah-google-reviews' ),
                    'type'       => Controls_Manager::SLIDER,
                    'size_units' => ['px', '%'],
                    'range'      => [
                        'px' => [
                            'min' => -3000,
                            'max' => 3000,
                        ],
                        '%'  => [
                            'min' => -100,
                            'max' => 100,
                        ],
                    ],
                    'default'    => [
                        'unit' => 'px',
                        'size' => 30,
                    ],
                    'selectors'  => [
                        '{{WRAPPER}} .abdullah-google-reviews-carousel-wrapper .abdullah-carousel-nav-prev' => 'left: {{SIZE}}{{UNIT}};',
                    ],
                ]
            );

            $this->add_responsive_control(
                'abdullah_google_reviews_carousel_prev_arrow_position_y_offset',
                [
                    'label'      => __( 'Y Offset', 'abdullah-google-reviews' ),
                    'type'       => Controls_Manager::SLIDER,
                    'size_units' => ['px', '%'],
                    'range'      => [
                        'px' => [
                            'min' => -3000,
                            'max' => 3000,
                        ],
                        '%'  => [
                            'min' => -100,
                            'max' => 100,
                        ],
                    ],
                    'default'    => [
                        'unit' => '%',
                        'size' => 50,
                    ],
                    'selectors'  => [
                        '{{WRAPPER}} .abdullah-google-reviews-carousel-wrapper .abdullah-carousel-nav-prev' => 'top: {{SIZE}}{{UNIT}};',
                    ],
                ]
            );

        $this->end_popover();

        $this->add_control(
            'abdullah_google_reviews_carousel_next_arrow_position',
            [
                'label'        => __( 'Next Arrow Position', 'abdullah-google-reviews' ),
                'type'         => Controls_Manager::POPOVER_TOGGLE,
                'label_off'    => __( 'Default', 'abdullah-google-reviews' ),
                'label_on'     => __( 'Custom', 'abdullah-google-reviews' ),
                'return_value' => 'yes',
                'default'      => 'yes',
            ]
        );
        
        $this->start_popover();

            $this->add_responsive_control(
                'abdullah_google_reviews_carousel_next_arrow_position_x_offset',
                [
                    'label'      => __( 'X Offset', 'abdullah-google-reviews' ),
                    'type'       => Controls_Manager::SLIDER,
                    'size_units' => ['px', '%'],
                    'range'      => [
                        'px' => [
                            'min' => -3000,
                            'max' => 3000,
                        ],
                        '%'  => [
                            'min' => -100,
                            'max' => 100,
                        ],
                    ],
                    'default'    => [
                        'unit' => 'px',
                        'size' => 30,
                    ],
                    'selectors'  => [
                        '{{WRAPPER}} .abdullah-google-reviews-carousel-wrapper .abdullah-carousel-nav-next' => 'right: {{SIZE}}{{UNIT}};',
                    ],
                ]
            );

            $this->add_responsive_control(
                'abdullah_google_reviews_carousel_next_arrow_position_y_offset',
                [
                    'label'      => __( 'Y Offset', 'abdullah-google-reviews' ),
                    'type'       => Controls_Manager::SLIDER,
                    'size_units' => ['px', '%'],
                    'range'      => [
                        'px' => [
                            'min' => -3000,
                            'max' => 3000,
                        ],
                        '%'  => [
                            'min' => -100,
                            'max' => 100,
                        ],
                    ],
                    'default'    => [
                        'unit' => '%',
                        'size' => 50,
                    ],
                    'selectors'  => [
                        '{{WRAPPER}} .abdullah-google-reviews-carousel-wrapper .abdullah-carousel-nav-next' => 'top: {{SIZE}}{{UNIT}};',
                    ],
                ]
            );

        $this->end_popover();

        $this->add_responsive_control(
            'abdullah_google_reviews_carousel_nav_arrow_radius',
            [
                'label'      => __( 'Border radius', 'abdullah-google-reviews' ),
                'type'       => Controls_Manager::DIMENSIONS,
                'size_units' => ['px', '%'],
                'default'    => [
                    'top'      => '50',
                    'right'    => '50',
                    'bottom'   => '50',
                    'left'     => '50',
                    'unit'     => 'px',
                    'isLinked' => true,
                ],
                'selectors'  => [
                    '{{WRAPPER}} .abdullah-google-reviews-carousel-wrapper .abdullah-carousel-nav-prev' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                    '{{WRAPPER}} .abdullah-google-reviews-carousel-wrapper .abdullah-carousel-nav-next' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],
            ]
        );

        $this->start_controls_tabs( 'abdullah_google_reviews_carousel_nav_arrow_tabs' );

			// normal state rating
			$this->start_controls_tab( 'abdullah_google_reviews_carousel_nav_arrow_normal', [ 'label' => esc_html__( 'Normal', 'abdullah-google-reviews' ) ] );

                $this->add_control(
                    'abdullah_google_reviews_carousel_arrow_normal_background',
                    [
                        'label'     => __( 'Background Color', 'abdullah-google-reviews' ),
                        'type'      => Controls_Manager::COLOR,
                        'default'   =>  $abdullah_primary_color,
                        'selectors' => [
                            '{{WRAPPER}} .abdullah-google-reviews-carousel-wrapper .abdullah-carousel-nav-prev' => 'background: {{VALUE}}',
                            '{{WRAPPER}} .abdullah-google-reviews-carousel-wrapper .abdullah-carousel-nav-next' => 'background: {{VALUE}}',
                        ],
                    ]
                );

                $this->add_control(
                    'abdullah_google_reviews_carousel_arrow_normal_color',
                    [
                        'label'     => __( 'Icon Color', 'abdullah-google-reviews' ),
                        'type'      => Controls_Manager::COLOR,
                        'default'   => '#ffffff',
                        'selectors' => [
                            '{{WRAPPER}} .abdullah-google-reviews-carousel-wrapper .abdullah-carousel-nav-prev i' => 'color: {{VALUE}};',
                            '{{WRAPPER}} .abdullah-google-reviews-carousel-wrapper .abdullah-carousel-nav-next i' => 'color: {{VALUE}};',
                        ],
                    ]
                );

                $this->add_group_control(
                    Group_Control_Border::get_type(),
                    [
                        'name'     => 'abdullah_google_reviews_carousel_arrow_normal_border',
                        'label'    => __( 'Border', 'abdullah-google-reviews' ),
                        'selector' => '{{WRAPPER}} .abdullah-google-reviews-carousel-wrapper .abdullah-carousel-nav-prev, {{WRAPPER}} .abdullah-google-reviews-carousel-wrapper .abdullah-carousel-nav-next',
                    ]
                );

                $this->add_group_control(
                    Group_Control_Box_Shadow::get_type(),
                    [
                        'name'     => 'abdullah_google_reviews_carousel_arrow_normal_shadow',
                        'label'    => __( 'Box Shadow', 'abdullah-google-reviews' ),
                        'selector' => '{{WRAPPER}} .abdullah-google-reviews-carousel-wrapper .abdullah-carousel-nav-prev, {{WRAPPER}} .abdullah-google-reviews-carousel-wrapper .abdullah-carousel-nav-next',
                    ]
                );

			$this->end_controls_tab();

			// hover state rating
			$this->start_controls_tab( 'abdullah_google_reviews_carousel_nav_arrow_hover', [ 'label' => esc_html__( 'Hover', 'abdullah-google-reviews' ) ] );

                $this->add_control(
                    'abdullah_google_reviews_carousel_arrow_hover_background',
                    [
                        'label'     => __( 'Background Color', 'abdullah-google-reviews' ),
                        'type'      => Controls_Manager::COLOR,
                        'default'   => $abdullah_primary_color,
                        'selectors' => [
                            '{{WRAPPER}} .abdullah-google-reviews-carousel-wrapper .abdullah-carousel-nav-prev:hover' => 'background: {{VALUE}}',
                            '{{WRAPPER}} .abdullah-google-reviews-carousel-wrapper .abdullah-carousel-nav-next:hover' => 'background: {{VALUE}}',
                        ],
                    ]
                );

                $this->add_control(
                    'abdullah_google_reviews_carousel_arrow_hover_color',
                    [
                        'label'     => __( 'Icon Color', 'abdullah-google-reviews' ),
                        'type'      => Controls_Manager::COLOR,
                        'default'   => '#ffffff',
                        'selectors' => [
                            '{{WRAPPER}} .abdullah-google-reviews-carousel-wrapper .abdullah-carousel-nav-prev:hover i' => 'color: {{VALUE}}',
                            '{{WRAPPER}} .abdullah-google-reviews-carousel-wrapper .abdullah-carousel-nav-next:hover i' => 'color: {{VALUE}}',
                        ],
                    ]
                );

                $this->add_group_control(
                    Group_Control_Border::get_type(),
                    [
                        'name'     => 'abdullah_google_reviews_carousel_arrow_hover_border',
                        'label'    => __( 'Border', 'abdullah-google-reviews' ),
                        'selector' => '{{WRAPPER}} .abdullah-google-reviews-carousel-wrapper .abdullah-carousel-nav-prev:hover, {{WRAPPER}} .abdullah-google-reviews-carousel-wrapper .abdullah-carousel-nav-next:hover',
                    ]
                );

                $this->add_group_control(
                    Group_Control_Box_Shadow::get_type(),
                    [
                        'name'     => 'abdullah_google_reviews_carousel_arrow_hover_shadow',
                        'label'    => __( 'Box Shadow', 'abdullah-google-reviews' ),
                        'selector' => '{{WRAPPER}} .abdullah-google-reviews-carousel-wrapper .abdullah-carousel-nav-prev:hover, {{WRAPPER}} .abdullah-google-reviews-carousel-wrapper .abdullah-carousel-nav-next:hover',
                    ]
                );

			$this->end_controls_tab();

        $this->end_controls_tabs();

		$this->end_controls_section();

		/**
		 * Dots Style
		 */
		
		$this->start_controls_section(
            'abdullah_google_reviews_carousel_nav_dot',
            [
                'label'     => esc_html__( 'Dots', 'abdullah-google-reviews' ),
                'tab'       => Controls_Manager::TAB_STYLE,
                'condition' => [
                    'abdullah_google_reviews_carousel_nav' => ['nav-dots', 'both', 'dynamic-dots'],
                ],
            ]
        );

        $this->add_control(
            'abdullah_google_reviews_carousel_nav_dot_alignment',
            [
                'label'   => __( 'Alignment', 'abdullah-google-reviews' ),
                'type'    => Controls_Manager::CHOOSE,
                'options' => [
                    'abdullah-google-reviews-carousel-dots-left'   => [
                        'title' => __( 'Left', 'abdullah-google-reviews' ),
                        'icon'  => 'fa fa-align-left',
                    ],
                    'abdullah-google-reviews-carousel-dots-center' => [
                        'title' => __( 'Center', 'abdullah-google-reviews' ),
                        'icon'  => 'fa fa-align-center',
                    ],
                    'abdullah-google-reviews-carousel-dots-right'  => [
                        'title' => __( 'Right', 'abdullah-google-reviews' ),
                        'icon'  => 'fa fa-align-right',
                    ],
                ],
                'default' => 'abdullah-google-reviews-carousel-dots-center',
            ]
        );

        $this->add_responsive_control(
            'abdullah_google_reviews_carousel_dots_top_spacing',
            [
                'label'      => __( 'Top Spacing', 'abdullah-google-reviews' ),
                'type'       => Controls_Manager::SLIDER,
                'size_units' => ['px'],
                'range'      => [
                    'px' => [
                        'min' => -200,
                        'max' => 200,
                    ],
                ],
                'default'    => [
                    'unit' => 'px',
                    'size' => 0,
                ],
                'selectors'  => [
                    '{{WRAPPER}} .abdullah-google-reviews-carousel-wrapper .abdullah-swiper-pagination' => 'margin-top: {{SIZE}}{{UNIT}};',
                ],
            ]
        );

		$this->add_responsive_control(
			'abdullah_google_reviews_carousel_dots_spacing_btwn',
			[
				'label' => __( 'Dot Space', 'abdullah-google-reviews' ),
				'type' => Controls_Manager::SLIDER,
				'size_units' => [ 'px' ],
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'default' => [
					'unit' => 'px',
					'size' => 8,
				],
				'selectors' => [
					'{{WRAPPER}} .abdullah-google-reviews-carousel-wrapper .abdullah-swiper-pagination .swiper-pagination-bullet:not(:last-child)' => 'margin-right: {{SIZE}}{{UNIT}};',
				]
			]
		);

        $this->add_responsive_control(
            'abdullah_google_reviews_carousel_nav_dot_radius',
            [
                'label'      => __( 'Border Radius', 'abdullah-google-reviews' ),
                'type'       => Controls_Manager::DIMENSIONS,
                'size_units' => ['px', '%'],
                'default'    => [
                    'top'      => '0',
                    'right'    => '0',
                    'bottom'   => '0',
                    'left'     => '0',
                    'unit'     => 'px',
                    'isLinked' => true,
                ],
                'selectors'  => [
                    '{{WRAPPER}} .abdullah-google-reviews-carousel-wrapper .abdullah-swiper-pagination .swiper-pagination-bullet' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],
            ]
        );

        $this->start_controls_tabs( 'abdullah_google_reviews_carousel_nav_dots_tabs' );

			// normal state rating
            $this->start_controls_tab( 'abdullah_google_reviews_carousel_nav_dots_normal', [ 'label' => esc_html__( 'Normal', 'abdullah-google-reviews' ) ] );

                $this->add_responsive_control(
                    'abdullah_google_reviews_carousel_dots_normal_height',
                    [
                        'label'      => __( 'Height', 'abdullah-google-reviews' ),
                        'type'       => Controls_Manager::SLIDER,
                        'size_units' => ['px'],
                        'range'      => [
                            'px' => [
                                'min' => 0,
                                'max' => 100,
                            ],
                        ],
                        'default'    => [
                            'unit' => 'px',
                            'size' => 10,
                        ],
                        'selectors'  => [
                            '{{WRAPPER}} .abdullah-google-reviews-carousel-wrapper .abdullah-swiper-pagination .swiper-pagination-bullet' => 'height: {{SIZE}}{{UNIT}};',
                        ],
                    ]
                );

				$this->add_responsive_control(
                    'abdullah_google_reviews_carousel_dots_normal_width',
                    [
                        'label'      => __( 'Width', 'abdullah-google-reviews' ),
                        'type'       => Controls_Manager::SLIDER,
                        'size_units' => ['px'],
                        'range'      => [
                            'px' => [
                                'min' => 0,
                                'max' => 100,
                            ],
                        ],
                        'default'    => [
                            'unit' => 'px',
                            'size' => 10,
                        ],
                        'selectors'  => [
                            '{{WRAPPER}} .abdullah-google-reviews-carousel-wrapper .abdullah-swiper-pagination .swiper-pagination-bullet' => 'width: {{SIZE}}{{UNIT}};',
                        ],
                    ]
                );

                $this->add_control(
                    'abdullah_google_reviews_carousel_dots_normal_background',
                    [
                        'label'     => __( 'Background Color', 'abdullah-google-reviews' ),
                        'type'      => Controls_Manager::COLOR,
                        'default'   => '#e5e5e5',
                        'selectors' => [
                            '{{WRAPPER}} .abdullah-google-reviews-carousel-wrapper .abdullah-swiper-pagination .swiper-pagination-bullet' => 'background: {{VALUE}};',
                        ],
                    ]
                );
				
				$this->add_responsive_control(
                    'abdullah_google_reviews_carousel_dots_normal_opacity',
                    [
                        'label'     => __( 'Opacity', 'abdullah-google-reviews' ),
                        'type'      => Controls_Manager::SLIDER,
                        'default' => [
							'size' => 1,
						],
						'range' => [
							'px' => [
								'max' => 1,
								'step' => 0.01,
							],
						],
                        'selectors' => [
                            '{{WRAPPER}} .abdullah-google-reviews-carousel-wrapper .abdullah-swiper-pagination .swiper-pagination-bullet' => 'opacity: {{SIZE}};',
                        ],
                    ]
                );

                $this->add_group_control(
                    Group_Control_Border::get_type(),
                    [
                        'name'     => 'abdullah_google_reviews_carousel_dots_normal_border',
                        'label'    => __( 'Border', 'abdullah-google-reviews' ),
                        'selector' => '{{WRAPPER}} .abdullah-google-reviews-carousel-wrapper .abdullah-swiper-pagination .swiper-pagination-bullet',
                    ]
                );

			$this->end_controls_tab();

			// hover state rating
            $this->start_controls_tab( 'abdullah_google_reviews_carousel_nav_dots_hover', [ 'label' => esc_html__( 'Hover/active', 'abdullah-google-reviews' ) ] );
            
                $this->add_responsive_control(
                    'abdullah_google_reviews_carousel_dots_active_height',
                    [
                        'label'      => __( 'Height', 'abdullah-google-reviews' ),
                        'type'       => Controls_Manager::SLIDER,
                        'size_units' => ['px', '%'],
                        'range'      => [
                            'px' => [
                                'min' => 0,
                                'max' => 100,
                            ],
                        ],
                        'default'    => [
                            'unit' => 'px',
                            'size' => 10,
                        ],
                        'selectors'  => [
                            '{{WRAPPER}} .abdullah-google-reviews-carousel-wrapper .abdullah-swiper-pagination .swiper-pagination-bullet-active' => 'height: {{SIZE}}{{UNIT}};',
                        ],
                    ]
                );

				$this->add_responsive_control(
                    'abdullah_google_reviews_carousel_dots_active_width',
                    [
                        'label'      => __( 'Width', 'abdullah-google-reviews' ),
                        'type'       => Controls_Manager::SLIDER,
                        'size_units' => ['px', '%'],
                        'range'      => [
                            'px' => [
                                'min' => 0,
                                'max' => 100,
                            ],
                        ],
                        'default'    => [
                            'unit' => 'px',
                            'size' => 10,
                        ],
                        'selectors'  => [
                            '{{WRAPPER}} .abdullah-google-reviews-carousel-wrapper .abdullah-swiper-pagination .swiper-pagination-bullet-active' => 'width: {{SIZE}}{{UNIT}};',
                        ],
                    ]
                );

                $this->add_control(
                    'abdullah_google_reviews_carousel_dots_active_background',
                    [
                        'label'     => __( 'Background Color', 'abdullah-google-reviews' ),
                        'type'      => Controls_Manager::COLOR,
                        'default'   => $abdullah_primary_color,
                        'selectors' => [
                            '{{WRAPPER}} .abdullah-google-reviews-carousel-wrapper .abdullah-swiper-pagination .swiper-pagination-bullet-active' => 'background: {{VALUE}};',
                            '{{WRAPPER}} .abdullah-google-reviews-carousel-wrapper .abdullah-swiper-pagination .swiper-pagination-bullet:hover' => 'background: {{VALUE}};',
                        ],
                    ]
                );

				$this->add_responsive_control(
                    'abdullah_google_reviews_carousel_dots_hover_opacity',
                    [
                        'label'     => __( 'Opacity', 'abdullah-google-reviews' ),
                        'type'      => Controls_Manager::SLIDER,
						'default' => [
							'size' => 1,
						],
						'range' => [
							'px' => [
								'max' => 1,
								'step' => 0.01,
							],
						],
                        'selectors' => [
							'{{WRAPPER}} .abdullah-google-reviews-carousel-wrapper .abdullah-swiper-pagination .swiper-pagination-bullet-active' => 'opacity: {{SIZE}};',
                            '{{WRAPPER}} .abdullah-google-reviews-carousel-wrapper .abdullah-swiper-pagination .swiper-pagination-bullet:hover' => 'opacity: {{SIZE}};',
                        ],
                    ]
                );

                $this->add_group_control(
                    Group_Control_Border::get_type(),
                    [
                        'name'     => 'abdullah_google_reviews_carousel_dots_active_border',
                        'label'    => __( 'Border', 'abdullah-google-reviews' ),
                        'selector' => '{{WRAPPER}} .abdullah-google-reviews-carousel-wrapper .abdullah-swiper-pagination .swiper-pagination-bullet-active, {{WRAPPER}} .abdullah-google-reviews-carousel-wrapper .abdullah-swiper-pagination .swiper-pagination-bullet:hover',
                    ]
                );

			$this->end_controls_tab();

        $this->end_controls_tabs();

        $this->end_controls_section();
    }

	public function render_api_script(){ 
		$api_key = $this->get_settings( 'abdullah_google_google_map_api_key' );
		wp_enqueue_script( 'abdullah-google-map-api', 'https://maps.googleapis.com/maps/api/js?key='. $api_key, array(), '1.0.0', false );
    }

    public function get_transient_expire( $settings ) {

        $expire_value = $settings['abdullah_reload_reviews'];
        $expire_time  = 24 * HOUR_IN_SECONDS;

        if ( 'abdullah-hour' === $expire_value ) {
            $expire_time = 60 * MINUTE_IN_SECONDS;
        } elseif ( 'abdullah-week' === $expire_value ) {
            $expire_time = 7 * DAY_IN_SECONDS;
        } elseif ( 'abdullah-month' === $expire_value ) {
            $expire_time = 30 * DAY_IN_SECONDS;
        } elseif ( 'abdullah-year' === $expire_value ) {
            $expire_time = 365 * DAY_IN_SECONDS;
        }

        return $expire_time;
    }

    public function get_transient_key( $place_id ){
        $place_id = strtolower( $place_id );
        $transient = 'google_reviews_data_' . $place_id;
        return $transient;
    }

    public function get_api_url( $api_key, $place_id ){
        $url = $this->google_place_url . 'details/json?placeid=' . $place_id . '&key=' . $api_key;
        return $url;
    }

	public function get_cache_data( $place_id ){
		$settings   = $this->get_settings_for_display();
		
        $transient = $this->get_transient_key( $place_id );
        $data      = get_transient( $transient );

        if($settings['abdullah_google_reviews_clear_cache'] != 'yes'){
        	 delete_transient( $transient );
        }

        if(is_array( $data ) && count( $data ) > 0){
            if( $place_id == $data['place_id'] ){
                return $data;
            } else {
                delete_transient($transient);
            }
        }
        return false;
	}

    public function getReviews(){

        $settings   = $this->get_settings_for_display();
        $place_id = $settings['abdullah_google_place_id'];
        $api_key  = $settings['abdullah_google_google_map_api_key'];

        if(!$place_id || !$api_key){
            return false;
        }
 
        $reviewData = $this->get_cache_data( $place_id );

        if( $reviewData ){
            return $reviewData;
        }else{
            $requestUrl = $this->get_api_url( $api_key, $place_id );  

            $response = wp_remote_get( $requestUrl );

            if (is_wp_error( $response ) ) {
                return array( 'error_message'=>$response->get_error_message() );
            }
            $response   = json_decode( $response[ 'body' ],true );
            $result     = ( isset($response['result']) && is_array( $response['result'] ) )?$response['result']:'';

            if(is_array( $result )){

                if(isset( $result['error_message'] ) ){
                    return $result;
                }

                $transient = $this->get_transient_key( $place_id );
                $expireTime = $this->get_transient_expire( $settings );

                set_transient( $transient, $result, $expireTime );
                return $result;
            }
            return $response;
        }
    }

    /**
	 * Rating content.
	 */
	private function render_google_reviews_rating( $ratings, $client_rating ) {
		$settings = $this->get_settings_for_display();
		$rating_output = '';

		$rating_output .= '<div class="abdullah-google-reviews-carousel-ratings">';
			$rating_output .= '<ul class="abdullah-google-reviews-ratings">';
				for( $rating = 1; $rating <= 5; $rating++ ) {
					if( $client_rating >= $rating ) {
						$rating_output .= '<li class="abdullah-google-reviews-ratings-active"><i class="'.$settings['abdullah_google_reviews_rating_icon']['value'].'"></i></li>';
					}else {
						$rating_output .= '<li><i class="'.$settings['abdullah_google_reviews_rating_icon']['value'].'"></i></li>';
					}
				}
			$rating_output .= '</ul>';
		$rating_output .= '</div>';

		return $rating_output;
	}

	/**
	 * Reviewer Thumbnails.
	 * 
	 */
	private function render_google_reviews_thumb( $settings, $client_name, $client_url, $client_photoUrl ) {
		$settings = $this->get_settings_for_display();
		$thumb_output = '';

		$thumb_output .= '<div class="abdullah-google-reviews-thumb">';
			$thumb_output .= '<a href="' . esc_url( $client_url ) . '" target="_blank">';
				$thumb_output .= '<img src="' . esc_url($client_photoUrl) . '" alt="' . esc_html($client_name) . '">';
			$thumb_output .= '</a>';
		$thumb_output .= '</div>';

		return $thumb_output;
	}

	 /**
	 * Reviewer content.
	 * ( name, description, rating, review date ) etc.
	 */
	private function render_google_reviews_content( $settings, $client_name, $client_rating, $client_url, $client_photoUrl, $human_time ) {
		$settings = $this->get_settings_for_display();
		$content_output = '';

		$content_output .= '<div class="abdullah-google-reviews-reviewer">';
			if ( $settings['abdullah_google_reviews_show_name'] == 'yes' ) :
				$content_output .= '<h4 class="abdullah-author-name">';
					$content_output .= '<a href="'. esc_url( $client_url ) . '">' . esc_html( $client_name ) . '</a>';
				$content_output .= '</h4>';
			endif;
			if ( $settings['abdullah_google_reviews_show_date'] == 'yes' ) :
				$content_output .= '<div class="abdullah-google-reviews-date">' . esc_html( date("M d Y", strtotime( $human_time ) ) ) . '</div>';
			endif;
		if ( 'yes' === $settings['abdullah_google_reviews_show_rating'] && 'below-author-name' == $settings['abdullah_google_reviews_rating_layout']) :
			$content_output .=  $this->render_google_reviews_rating( $settings['abdullah_google_reviews_rating_icon'], $client_rating );
		endif;
		$content_output .= '</div>';

		if ( $settings['abdullah_google_reviews_show_user_image'] == 'yes' && 'abdullah-google-reviews-align-bottom' == $settings['abdullah_google_reviews_carousel_container_alignment'] ) :
		$content_output .= '<div class="abdullah-google-reviews-thumb">';
			$content_output .= '<a href="<?php echo esc_url( $client_url ); ?>" target="_blank">';
				$content_output .= '<img src="' . esc_url($client_photoUrl) .'" alt="' . esc_html($client_name) . '">';
			$content_output .= '</a>';
		$content_output .= '</div>';
		endif;
		
		return $content_output;
	}

    protected function render() {
        $settings = $this->get_settings_for_display();
        $direction = is_rtl() ? 'true' : 'false';
        $place_id = $settings['abdullah_google_place_id'];
        $api_key  = $settings['abdullah_google_google_map_api_key'];
        $reviewData  = $this->getReviews();
        $GReviews = isset($reviewData['reviews']) ? $reviewData['reviews']: [];

		$elementor_viewport_lg = get_option( 'elementor_viewport_lg' );
		$elementor_viewport_md = get_option( 'elementor_viewport_md' );
		$abdullah_viewport_lg     = !empty($elementor_viewport_lg) ? $elementor_viewport_lg - 1 : 1023;
		$abdullah_viewport_md     = !empty($elementor_viewport_md) ? $elementor_viewport_md - 1 : 767;

        $this->add_render_attribute(
			'abdullah-google-reviews-wrapper',
			[	
				'id' => "abdullah-facebook-feed-wrapper",
				'class' => ""
			]
		);

		if ( !empty( $api_key ) ):

			$this->add_render_attribute( 
				'abdullah-google-reviews-carousel', 
				[ 
					'class'               => [ 'abdullah-google-reviews-carousel-wrapper abdullah-google-reviews-carousel abdullah-carousel-item' ],
				]
			);
	
			$carousel_data_settings = wp_json_encode(
				array_filter([
					"autoplay"           	=> $settings["abdullah_google_reviews_autoplay"] ? true : false,
					"delay" 				=> $settings["abdullah_google_reviews_autoplay_speed"] ? true : false,
					"loop"           		=> $settings["abdullah_google_reviews_loop"] ? true : false,
					"speed"       			=> $settings["abdullah_google_reviews_transition_duration"],
					"pauseOnHover"       	=> $settings["abdullah_google_reviews_pause"] ? true : false,
					"slidesPerView"         => (int) $settings["abdullah_google_reviews_slider_per_view_mobile"],
					"slidesPerColumn" 		=> ($settings["abdullah_google_reviews_slides_per_column"] > 1) ? $settings["abdullah_google_reviews_slides_per_column"] : false,
					"centeredSlides"        => $settings["abdullah_google_reviews_slide_centered"] ? false : true,
					"spaceBetween"   		=> $settings['abdullah_google_reviews_column_space']['size'],
					"grabCursor"  			=> ($settings["abdullah_google_reviews_grab_cursor"] === "yes") ? true : false,
					"observer"       		=> ($settings["abdullah_google_reviews_observer"]) ? true : false,
					"observeParents" 		=> ($settings["abdullah_google_reviews_observer"]) ? true : false,
					"breakpoints"     		=> [

						(int) $abdullah_viewport_md 	=> [
							"slidesPerView" 	=> (int) $settings["abdullah_google_reviews_slider_per_view_tablet"],
							"spaceBetween"  	=> $settings["abdullah_google_reviews_column_space"]["size"],
							
						],
						(int) $abdullah_viewport_lg 	=> [
							"slidesPerView" 	=> (int) $settings["abdullah_google_reviews_slider_per_view"],
							"spaceBetween"  	=> $settings["abdullah_google_reviews_column_space"]["size"],
							
						]
					],
					"pagination" 			 	=>  [ 
						"el" 				=> ".abdullah-swiper-pagination",
						"type"       		=> "bullets",
			      		"clickable"  		=> true,
						'dynamicBullets' 	=> ( $settings["abdullah_google_reviews_carousel_nav"] == "dynamic-dots") ? true : false,
					],
					"navigation" => [
						"nextEl" => ".abdullah-carousel-nav-next",
						"prevEl" => ".abdullah-carousel-nav-prev",
					],

				])
			);
			$this->add_render_attribute( 'abdullah-google-reviews-carousel', 'data-settings',  $carousel_data_settings );
			$this->add_render_attribute( 'abdullah-google-reviews-carousel', 'class', esc_attr( $settings['abdullah_google_reviews_carousel_nav_dot_alignment'] ) );
		endif;

		$this->add_render_attribute( 'abdullah_google_reviews_content_wrapper', 'class', 'abdullah-google_reviews-content-wrapper' );

        ?>

        <div <?php echo $this->get_render_attribute_string( 'abdullah-google-reviews-wrapper' ); ?>>
            <div class="abdullah-google-reviews-items">
				<div <?php echo $this->get_render_attribute_string( 'abdullah-google-reviews-carousel' ); ?>>
					<div class="swiper-container">
						<div class="swiper-wrapper">
							<?php
								foreach( $GReviews as $review ){ 
									$client_name         = $review['author_name'];
									$client_url          = $review['author_url'];
									$client_photoUrl     = $review['profile_photo_url'];
									$human_time          = $review['relative_time_description'];
									$client_text         = $review['text'];
									$client_rating       = $review['rating'];


									$description = explode( ' ', $review['text'] );
									if ( !empty( $settings['abdullah_google_reviews_excerpt_limit'] ) && count( $description ) > $settings['abdullah_google_reviews_excerpt_limit'] ) {
										$description_shorten = array_slice( $description, 0, $settings['abdullah_google_reviews_excerpt_limit'] );
										$description = implode( ' ', $description_shorten ) . '...';
									} else {
										$description = $review['text'];
									}
									?>

									<div class="swiper-slide abdullah-google-reviews-item abdullah-google-reviews-wrapper <?php echo esc_attr( $settings['abdullah_google_reviews_carousel_container_alignment'] ) ;?>">
										<?php if( 'layout-2' === $settings['abdullah_google_reviews_carousel_layout'] ){ ?>
											<div class="abdullah-google-reviews-reviewer-wrapper">
												<?php if ( $settings['abdullah_google_reviews_show_user_image'] == 'yes' && 'abdullah-google-reviews-align-bottom' !== $settings['abdullah_google_reviews_carousel_container_alignment'] ) : ?>

													<?php echo $this->render_google_reviews_thumb( $settings, $client_name, $client_url, $client_photoUrl ); ?>

												<?php endif ?>

												<?php echo $this->render_google_reviews_content( $settings, $client_name, $client_rating, $client_url, $client_photoUrl, $human_time ); ?>

											</div>
										<?php }; ?>
										<?php if ( 'yes' === $settings['abdullah_google_reviews_show_rating'] && 'above-description' == $settings['abdullah_google_reviews_rating_layout']) : ?>
											<?php echo $this->render_google_reviews_rating( $settings['abdullah_google_reviews_rating_icon'], $client_rating ); ?>
										<?php endif;?>
										<div class="abdullah-google-reviews-wrapper-inner">
											<div class="abdullah-google-reviews-content-wrapper">
												<?php if ( 'yes' == $settings['abdullah_google_reviews_show_excerpt'] ) : ?>
													<div class="abdullah-google-reviews-description">
														<p><?php echo wp_kses_post($description); ?></p>
													</div>
												<?php endif; ?>
											</div>
											
											<?php if ( 'yes' === $settings['abdullah_google_reviews_show_rating'] && 'below-description' == $settings['abdullah_google_reviews_rating_layout']) : ?>
												<?php echo $this->render_google_reviews_rating( $settings['abdullah_google_reviews_rating_icon'], $client_rating ); ?>
											<?php endif;?>
											
											<?php if( 'layout-1' === $settings['abdullah_google_reviews_carousel_layout'] ){ ?>
												<div class="abdullah-google-reviews-reviewer-wrapper">
													<?php if ( $settings['abdullah_google_reviews_show_user_image'] == 'yes' && 'abdullah-google-reviews-align-bottom' !== $settings['abdullah_google_reviews_carousel_container_alignment'] ) : ?>

														<?php echo $this->render_google_reviews_thumb( $settings, $client_name, $client_url, $client_photoUrl ); ?>

													<?php endif ?>

													<?php echo $this->render_google_reviews_content( $settings, $client_name, $client_rating, $client_url, $client_photoUrl, $human_time ); ?>

												</div>
											<?php }; ?>
										</div>
									</div>

								<?php } ;
							?>
						</div>
						<?php if ( !empty( $api_key ) ):
							if( $settings['abdullah_google_reviews_carousel_nav'] == "arrows" || $settings['abdullah_google_reviews_carousel_nav'] == "both" ) : ?>
								<div class="abdullah-carousel-nav-next"><i class="fa fa-angle-right"></i></div>
								<div class="abdullah-carousel-nav-prev"><i class="fa fa-angle-left"></i></div>
							<?php endif; ?>
							<?php if( $settings['abdullah_google_reviews_carousel_nav'] == "nav-dots" || $settings['abdullah_google_reviews_carousel_nav'] == "both" || $settings["abdullah_google_reviews_carousel_nav"] == "dynamic-dots") : ?>
								<div class="abdullah-dots-container">
									<div class="abdullah-swiper-pagination"></div>
								</div>
							<?php endif; ?>
						<?php endif; ?>
					</div>
				</div>
            </div>
        </div>
		
    <?php 
	add_action( 'wp_footer', array( $this, 'render_api_script') );
    }


}