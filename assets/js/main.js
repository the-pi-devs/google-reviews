(function ($) {
    "use strict";

    var editMode = false;

    // Google Reviews Carousel
    var abdullahGoogleReviews = function( $scope, $ ) {

        var $slider = $scope.find( '.abdullah-google-reviews-carousel-wrapper' );
            
            if ( ! $slider.length ) {
                return;
            }
    
        var $sliderContainer = $slider.find('.swiper-container'),
        $settings 		 = $slider.data('settings');
    
        var swiper = new Swiper($sliderContainer, $settings);
    
        if ($settings.pauseOnHover) {
        $($sliderContainer).hover(function() {
            (this).swiper.autoplay.stop();
        }, function() {
            (this).swiper.autoplay.start();
        });
        }
    
  };

  $(window).on('elementor/frontend/init', function () {
    if( elementorFrontend.isEditMode() ) {
        editMode = true;
    }
    elementorFrontend.hooks.addAction( 'frontend/element_ready/abdullah-google-reviews.default', abdullahGoogleReviews );

});	

}(jQuery));